﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeThree
{
    public class Node
    {   
        private int key; // поле ключа
        private int data; // информационное поле 
        private Node left; // ссылка на левое поддерево 
        private Node right; // ссылка на правое поддерево 


        public Node() // конструкторы 
        {

        }

        public Node(int key, int inputDataNode)
        {
           Key = key; Data = inputDataNode; 
        }

        public Node(int key, int data, Node left, Node right)
        {   
            Key = key;
            Data = data;
            Left = left;
            Right = right;
            
        }
        public int Key { get; set; } // свойства 
        public int Data { get; set; }
            
        public Node Left { get; set; }
        public TreeThree.Node Right { get; set; }
    }
    public class TreeNode
    {
        private Node root; // ссылка на корень дерева  
        List<int> number = new List<int>(); // список  значения информационных полей, которые совпадают по значениям ключей

        public Node Root { get; set; }  // свойство, открывающее доступ к корню дерева 

        public Node AddNode(int key, int inputDataNode, Node root) // создание дерева
        {
            if (root == null)
            {
                root = new Node(key, inputDataNode);
            }
            else
            {
                if (key < root.Key)
                {
                    root.Left = AddNode(key, inputDataNode, root.Left);
                }
                else
                {
                    root.Right = AddNode(key, inputDataNode, root.Right);
                }
            }

            return root;
        }



        public void PrintTree(int x, int y, Node root, int delta = 0) // вывод на консоль
        {
            if (root != null)
            {
                if (delta == 0) delta = x / 2;
                Console.SetCursorPosition(x, y);
                
                Console.Write("ключ:"+ root.Key + " значение(" + root.Data + ")");
                PrintTree(x - delta, y + 3, root.Left, delta / 2);
                PrintTree(x + delta, y + 3, root.Right, delta / 2);
            }

        }
        public List<int> WriteTree(TreeNode tree, List<int> unique) // заполнение значениями
        {
        
            Random random = new Random();
            int n = random.Next(3, 6);
            for (int i = 0; i < n;)
            {
                int k = random.Next(2, 10);
                if (unique.Contains(k)) // проверка уникальности ключа
                {
                    continue;
                }
                else
                {
                    unique.Add(k);
                    i++;
                    tree.Root = tree.AddNode(k, random.Next(-10, 10), tree.Root);

                }

            }
            return unique; // ключи
        }
        public void FindElement(Node root,int key) // поиск значения по ключу
        {
            if (root == null)
            {
                root = new Node(key, 0, null, null);
            }
            else {
                if (key < root.Key)
                    FindElement(root.Left, key);
                else if (key > root.Key)
                    FindElement(root.Right, key);
                else if(key == root.Key)
                {
                    number.Add(root.Data); // Скопировать в линейную структуру по заданию
                    Console.WriteLine(root.Data + " - значение информационного поля");
                }
            }
        }


    }

}

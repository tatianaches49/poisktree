﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeTwo
{
    public class Node
    {
        private int key; // поле ключа
        private int data; // информационное поле  
        private Node left; // ссылка на левое поддерево  
        private Node right;  // ссылка на правое поддерево 


        public Node() // конструкторы 
        {

        }

        public Node(int key, int inputDataNode)
        {
            Key = key; Data = inputDataNode;
        }

        public Node(int key, int data, Node left, Node right)
        {
            Key = key;
            Data = data;
            Left = left;
            Right = right;

        }
        public int Key { get; set; } // свойства 
        public int Data { get; set; }
        public Node Left { get; set; }
        public TreeTwo.Node Right { get; set; }
    }
    public class TreeNode
    {
        private Node root; // ссылка на корень дерева  
        List<int> number = new List<int>(); // список, для отрицательных значений

        public Node Root { get; set; } // свойство, открывающее доступ к корню дерева 

        public Node AddNode(int key, int inputDataNode, Node root) // создание дерева
        {
            if (root == null)
            {
                root = new Node(key, inputDataNode);
            }
            else
            {
                if (key < root.Key)
                {
                    root.Left = AddNode(key, inputDataNode, root.Left);
                }
                else
                {
                    root.Right = AddNode(key, inputDataNode, root.Right);
                }
            }

            return root;
        }



        public void PrintTree(int x, int y, Node root, int delta = 0) // вывод на консоль
        {
            if (root != null)
            {
                if (delta == 0) delta = x / 2;
                Console.SetCursorPosition(x, y);

                Console.Write(root.Key + "(" + root.Data + ")");
                PrintTree(x - delta, y + 3, root.Left, delta / 2);
                PrintTree(x + delta, y + 3, root.Right, delta / 2);
            }

        }
        public void WriteTree(TreeNode tree) // заполнение значениями
        {
            List<int> unique = new List<int>();
            Random random = new Random();
            int n = random.Next(3, 6);
            Console.WriteLine("\n Рисуем дерево, с количеством узлов = " + n);
            for (int i = 0; i < n;)
            {
                int k = random.Next(2, 10);
                if (unique.Contains(k))  // проверка уникальности ключа
                {
                    continue;
                }
                else
                {
                    unique.Add(k);
                    i++;
                    tree.Root = tree.AddNode(k, random.Next(-10, 10), tree.Root); // диапазон с отрицательными значениями (3 задание)

                }

            }
         
        }

        public List<int> List(Node root) // получение отрицательных чисел
        {

            if (root != null)    
            {  
                if (root.Data >= 0) {  }

                else { number.Add(root.Data); } // наполнение

                List(root.Left);    
                List(root.Right);    
            }
            return number;

        }


        

    }

}

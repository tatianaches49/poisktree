﻿using System;

namespace Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            TreeNode tree = new TreeNode(); // объявление объекта класса

            tree.WriteTree(tree); // построение дерева

            tree.PrintTree(Console.WindowWidth / 2, 3, tree.Root); // рисовка в консоле
            Console.SetCursorPosition(0, 20); // установка позиции  начала записи
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Сумма значений информационных полей дерева = "+ tree.SummaElements(tree.Root)); //  (1 задание)
             Console.WriteLine("Количество внутренних узлов = "+ (tree.CountElements(tree.Root) - 1)); //  (-1 для корня) (2 задание)
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace TreeThree
{
    class Program
    {
        static void Main(string[] args)
        {
            TreeNode tree = new TreeNode(); // объявление объекта класса для 1 дерева
            List<int> unique = new List<int>(); // список ключей
            unique = tree.WriteTree(tree, unique); // получение
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Рисуем первое дерево");
            Console.ForegroundColor = ConsoleColor.White;
            tree.PrintTree(Console.WindowWidth / 2, 2, tree.Root); //рисовка в консоле

            TreeNode tree2 = new TreeNode(); // объявление объекта класса для 2 дерева
            List<int> unique2 = new List<int>();
            unique2 = tree2.WriteTree(tree2, unique2);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\nРисуем второе дерево");
            Console.ForegroundColor = ConsoleColor.White;
            tree2.PrintTree(Console.WindowWidth / 2, 15, tree2.Root);

            Console.SetCursorPosition(0, 30);

            // совпадения определяются в списке наибольшего размера
            if (unique.Count < unique2.Count) {
                foreach (int u in unique) {
                    if (unique2.Contains(u)) {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine(u + " - совпадающий ключ");
                        Console.ForegroundColor = ConsoleColor.White;
                        // поиск в 2ух деревьях
                        tree.FindElement(tree.Root, u);
                      tree2.FindElement(tree2.Root, u);
                    }
                }
            } else {
                foreach (int u in unique2)
                {
                    if (unique.Contains(u))
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine(u+ " -  совпадающий ключ");
                        Console.ForegroundColor = ConsoleColor.White;
                        // поиск в 2ух деревьях
                        tree.FindElement(tree.Root, u);
                        tree2.FindElement(tree2.Root, u);
                    }
                }

            }
        }

   
    }
}

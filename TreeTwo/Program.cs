﻿using System;
using System.Collections.Generic;

namespace TreeTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            TreeNode tree = new TreeNode(); // объявление объекта класса

            tree.WriteTree(tree); // построение дерева

            tree.PrintTree(Console.WindowWidth / 2, 2, tree.Root); //рисовка в консоле
            Console.SetCursorPosition(0, 20); // установка позиции  начала записи

            List<int> num = tree.List(tree.Root); // получение list
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Список отрицательных значений информационных полей дерева: ");
            foreach (int c in num) { // перебор
                Console.Write(c + " "); // вывод
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
        }
}
